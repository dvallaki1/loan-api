<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Middleware\Admin;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Authentication API Routes
Route::post('/login', 'ApiController@login');
Route::get('/logout', 'ApiController@logout');
Route::post('/register', 'ApiController@register');


Route::middleware('auth:sanctum')->group(function () {

    Route::get('my-loans', 'ApiController@myLoans');
    Route::post('create-loan', 'ApiController@createLoan');
    Route::post('pay-loan-payment', 'ApiController@payLoanPayment');

    //admin routes
    Route::middleware(Admin::class)->group(function () {
	    Route::get('all-loans', 'ApiController@allLoans');
	    Route::post('approve-loan', 'ApiController@approveLoan');
	});
});
