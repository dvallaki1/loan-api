<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert([
            [  'name' => 'Admin',
                'email' => 'admin@yopmail.com',
                'password' => Hash::make('Admin@123'),
                'user_type' => 'admin',
                'created_at' => date("Y-m-d h:i:s")],
            [  'name' => 'User1 Test',
                'email' => 'user1@yopmail.com',
                'password' => Hash::make('123456789'),
                'user_type' => 'user',
                'created_at' => date("Y-m-d h:i:s")],
            [  'name' => 'User2 Test',
                'email' => 'user2@yopmail.com',
                'password' => Hash::make('123456789'),
                'user_type' => 'user',
                'created_at' => date("Y-m-d h:i:s")]
        ]);
    }
}
