<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Hash;
use App\Models\User;
use App\Models\Loan;
use App\Models\LoanPayment;

class ApiController extends Controller
{
    public function register(Request $request)
    {
        $formData = request()->except(['_token']);

        $password = $formData['password'];

        $rules = [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required','email','unique:users'],
                'password' => 'required|min:8',
            ];

        $messages = [
            "name.required" => "Please enter :attribute",
            "email.required" => "Please enter email",
        ];    

        $validator = Validator::make($formData, $rules, $messages);
        
        if ($validator->fails()) {
            return response(['status' => 400, 'msg' => $validator->errors()->first()], 400);
        }

        $formData['password'] = Hash::make($password);
        $formData['user_type'] = 'user';

        // save the user to the database

        $user = User::create($formData);

        if($user)
        {
            $token = $user->createToken('auth_token')->plainTextToken;
            return response([
                'status' => 200,
                'msg' => 'Registration successfull.'
            ], 200);
        }
        else {
            return response([
                'status' => 400,
                'msg' => 'Error occured!',
            ],400);
        }
    }

    public function login(Request $request)
    {
        // Validate the form data
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);

        $formData = request()->except(['_token']);
         
        $user = User::whereEmail($formData['email'])->first();
        
        if ($user) {
	        if (auth()->attempt(['email' => $request->email, 'password' => $request->password])) {
	    
	            $token = $user->createToken('auth_token')->plainTextToken;

	            return response([
	                'status' => 200,
	                'user' => $user,
	                'auth_token' => $token,
	            ], 200);
	        } else {
	            return response([
	                'status' => 400,
	                'msg' => 'Password not correct.',
	            ], 400);
	        }
        } else {
            return response([
                'status' => 400,
                'msg' => 'Sorry, there is no record of an account associated with this email. Please retry.',
            ], 400);
        }
    }

    public function myLoans(Request $request)
    {
    	$data = Loan::with('loan_payments')->where('user_id',$request->user()->id)->get();

    	if (count($data) > 0) {
            return response(['status' => 200, 'data' => $data], 200);
        } else {
            return response(['status' => 400, 'msg' => 'No loans found'], 400);
        }
    }

    public function createLoan(Request $request)
    {
    	$formData = request()->except(['_token']);

        $rules = [
                'amount' => ['required', 'numeric'],
                'loan_term' => ['required','numeric'],
            ];

        $messages = [
            "amount.required" => "Please enter :attribute",
            "loan_term.required" => "Please enter :attribute",
        ];    

        $validator = Validator::make($formData, $rules, $messages);
        
        if ($validator->fails()) {
            return response(['status' => 400, 'msg' => $validator->errors()->first()], 400);
        }

        $formData['user_id'] = $request->user()->id;

        $loan = Loan::create($formData);

        $date = date('Y-m-d');

        for($i=0;$i < $formData['loan_term'];$i++)
        {
        	$date = date('Y-m-d', strtotime("+1 week", strtotime($date)));
        	LoanPayment::create([
        		'loan_id' => $loan->id,
        		'amount' => $formData['amount']/$formData['loan_term'],
        		'payment_date' => $date
        	]);
        }

        return response(['status' => 200, 'msg' => 'Loan request submitted successfully.'], 200);
    }

    public function allLoans(Request $request)
    {
    	$data = Loan::with('user')->get();

    	if (count($data) > 0) {
            return response(['status' => 200, 'data' => $data], 200);
        } else {
            return response(['status' => 400, 'msg' => 'No loans found'], 400);
        }
    }

    public function approveLoan(Request $request)
    {
    	$loan = Loan::find($request->loan_id);

    	if($loan)
    	{
    		$loan->update(['status'=>'approved']);
    		return response(['status' => 200, 'msg' => 'Loan approved successfully.'], 200);
    	}
    	else
    		return response(['status' => 400, 'msg' => 'No loan found with this id.'], 400);
    }

    public function payLoanPayment(Request $request)
    {
    	$formData = request()->except(['_token']);

    	$rules = [
                'amount' => ['required', 'numeric'],
                'loan_id' => ['required','numeric'],
            ];

        $messages = [
            "amount.required" => "Please enter :attribute",
            "loan_id.required" => "Please enter :attribute",
        ];    

        $validator = Validator::make($formData, $rules, $messages);
        
        if ($validator->fails()) {
            return response(['status' => 400, 'msg' => $validator->errors()->first()], 400);
        }

        $loan = Loan::with('loan_payments')->find($formData['loan_id']);

        if($loan->loan_payments[0]->amount <= $formData['amount'])
        {
        	$loan->loan_payments->where('status','pending')->first()->update(['status'=>'paid']);
        	
        	$count = 0;

        	foreach ($loan->loan_payments as $loan_payment) {
        		if($loan_payment->status=='paid')
        			$count++;
        	}

        	if($count==$loan->loan_payments->count())
        		$loan->update(['status'=>'paid']);

        	return response(['status' => 200, 'msg' => 'Loan paid successfully.'], 200);
        }

        else
        	return response(['status' => 400, 'msg' => 'Loan amount should be greater or equal to installment amount.'], 400);
    }
}
